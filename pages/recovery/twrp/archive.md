---
layout: default
permalink: /recovery/twrp/archive/
title: TeamWin Recovery Project Build Archive
description: Older TWRP Images for your zenfone3 [ZE520KL / ZE552KL]
---

{% assign project = site.data.recovery.twrp %}

{% assign api = site.data.twrp.files %}
{% include api_gen.html %}
{% include templates/archive.html %}
