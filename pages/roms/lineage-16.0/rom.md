---
layout: default
permalink: /roms/lineage-16.0/
title: LineageOS 16.0
description: LineageOS 16.0 for your zenfone3 [ZE520KL / ZE552KL]
---

{% assign project = site.data.roms.lineage-160 %}

{% assign api = site.data.lineage-160.files %}
{% include api_gen.html %}
{% include templates/rom_info.html %}

