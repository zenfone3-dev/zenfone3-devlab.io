---
layout: default
permalink: /roms/lineage-16.0/archive/
title: LineageOS 16.0 Build Archive
description: Older LineageOS 16.0 builds for your zenfone3 [ZE520KL / ZE552KL]
---

{% assign project = site.data.roms.lineage-160 %}

{% assign api = site.data.lineage-160.files %}
{% include api_gen.html %}

<div class="container">
    <div class="alert alert-danger" role="alert">
        <strong><i class="fa fa-exclamation-triangle"></i> Warning: </strong> Builds before 22 Feb 2019 are signed with testkeys use with your own risk.
    </div>
</div>

{% include templates/archive.html %}